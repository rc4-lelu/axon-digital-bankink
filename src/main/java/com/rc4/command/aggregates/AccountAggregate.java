package com.rc4.command.aggregates;

import com.rc4.command.exceptions.BalanceNotSufficientException;
import com.rc4.coreapi.command.CreateAccountCommand;
import com.rc4.coreapi.command.CreditAccountCommand;
import com.rc4.coreapi.command.DebitAccountCommand;
import com.rc4.coreapi.enums.AccountStatus;
import com.rc4.coreapi.event.AccountActivatedEvent;
import com.rc4.coreapi.event.AccountCreatedEvent;
import com.rc4.coreapi.event.AccountCreditedEvent;
import com.rc4.coreapi.event.AccountDebitedEvent;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

import java.math.BigDecimal;

@Aggregate
@NoArgsConstructor // Default constructor required by axon
@Slf4j
public class AccountAggregate {

    @AggregateIdentifier
    private String accountId;
    private BigDecimal balance;
    private String currency;
    private AccountStatus status;

    @CommandHandler //Fonction de decision
    public AccountAggregate(CreateAccountCommand command){
        log.info("Created account command received...");

        // Dispacth event
        AggregateLifecycle.apply(new AccountCreatedEvent(
                command.getId(),
                command.getInitialBalance(),
                command.getCurrency()
        ));
    }

    /**
     * Update state of application
     */

    @EventSourcingHandler //Fonction d-évolution
    public void on(AccountCreatedEvent event){
        log.info("AccountCreatedEvent...");
        this.accountId = event.getId();
        this.balance = event.getInitialBalance();
        this.currency = event.getCurrency();
        this.status = AccountStatus.CREATED;
        AggregateLifecycle.apply(new AccountActivatedEvent(
                event.getId(),
                AccountStatus.ACTIVATED
        ));
    }

    @EventSourcingHandler //Fonction d-évolution
    public void on(AccountActivatedEvent event){
        log.info("AccountCreatedEvent...");
        this.status = event.getStatus();
    }

    @CommandHandler //Fonction de decision
    public void handler(CreditAccountCommand command){
        log.info("Created account command received...");

        // Dispacth event
        AggregateLifecycle.apply(new AccountCreditedEvent(
                command.getId(),
                command.getAmount(),
                command.getCurrency()
        ));
    }

    @EventSourcingHandler //Fonction d-évolution
    public void on(AccountCreditedEvent event){
        log.info("AccountCreditedEvent...");
        this.balance = this.balance.add(event.getAmount());
    }

    @CommandHandler //Fonction de decision
    public void handler(DebitAccountCommand command){
        log.info("Created account command received...");
       if (this.balance.subtract(command.getAmount()).doubleValue() < 0) {
            throw new BalanceNotSufficientException("Balance Not Sufficient Exception");
        }

        // Dispacth event
        AggregateLifecycle.apply(new AccountDebitedEvent(
                command.getId(),
                command.getAmount(),
                command.getCurrency()
        ));
    }

    @EventSourcingHandler //Fonction d-évolution
    public void on(AccountDebitedEvent event){
        log.info("AccountCreditedEvent...");
        this.balance = this.balance.subtract(event.getAmount());
    }
}
