package com.rc4.command.controllers;

import com.rc4.command.dto.CreateAccountRequestDto;
import com.rc4.command.dto.CreditAccountRequestDto;
import com.rc4.command.dto.DebitAccountRequestDto;
import com.rc4.coreapi.command.CreateAccountCommand;
import com.rc4.coreapi.command.CreditAccountCommand;
import com.rc4.coreapi.command.DebitAccountCommand;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

@RestController
@RequestMapping(path = "/command/accounts")
@Slf4j
@AllArgsConstructor
public class AccountCommandRestApi {

    private final CommandGateway commandGateway;
    private final EventStore eventStore;

    @PostMapping("/create")
    public CompletableFuture<String> newAccount(@RequestBody CreateAccountRequestDto request){
        log.info("CreateAccountRequestDto => " + request.getInitialBalance().toString());
        CompletableFuture<String> response = commandGateway.send(new CreateAccountCommand(
                UUID.randomUUID().toString(),
                request.getInitialBalance(),
                request.getCurrency()
        ));
        return response;
    }

    @PutMapping("/credit")
    public CompletableFuture<String> creditAccount(@RequestBody CreditAccountRequestDto request){
        log.info("CreditAccountRequestDto => ");
        CompletableFuture<String> response = commandGateway.send(new CreditAccountCommand(
                request.getAccountId(),
                request.getAmount(),
                request.getCurrency()
        ));
        return response;
    }

    @PutMapping("/debit")
    public CompletableFuture<String> debitAccount(@RequestBody DebitAccountRequestDto request){
        log.info("DebitAccountRequestDto => ");
        CompletableFuture<String> response = commandGateway.send(new DebitAccountCommand(
                request.getAccountId(),
                request.getAmount(),
                request.getCurrency()
        ));
        return response;
    }

    @GetMapping("/events/{accountId}")
    public Stream accountEvent(@PathVariable String accountId){
        return eventStore.readEvents(accountId).asStream();
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> exceptionHandler(Exception e){
        return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
