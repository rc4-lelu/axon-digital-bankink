package com.rc4.command.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data @AllArgsConstructor @NoArgsConstructor
public class CreateAccountRequestDto {
    private BigDecimal initialBalance;
    private String currency;
}

