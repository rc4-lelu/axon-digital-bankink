package com.rc4.query.queries;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data @AllArgsConstructor
public class GetAccountHistoryQuery {
    private final String accountId;

}
