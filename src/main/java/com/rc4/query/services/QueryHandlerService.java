package com.rc4.query.services;

import com.rc4.query.dto.AccountDto;
import com.rc4.query.dto.AccountHistoryDto;
import com.rc4.query.dto.OperationDto;
import com.rc4.query.entities.Account;
import com.rc4.query.entities.Operation;
import com.rc4.query.mappers.AccountMapper;
import com.rc4.query.queries.GetAccountByIdQuery;
import com.rc4.query.queries.GetAccountHistoryQuery;
import com.rc4.query.queries.GetAccountOperationQuery;
import com.rc4.query.repository.AccountRepository;
import com.rc4.query.repository.OperationRepository;
import lombok.AllArgsConstructor;

import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class QueryHandlerService {
    private final AccountRepository accountRepository;
    private final OperationRepository operationRepository;
    private final AccountMapper accountMapper;


    @QueryHandler
    public AccountDto handle(GetAccountByIdQuery query) {
        Account account = accountRepository.findById(query.getAccountId()).get();
        AccountDto accountDto = accountMapper.fromAccount(account);
        return accountDto;
    }

    @QueryHandler
    public List<OperationDto> handle(GetAccountOperationQuery query) {
        List<Operation> operations = operationRepository.findByAccountId(query.getAccountId());
        List<OperationDto> operationDtos = operations
                .stream()
                .map(operation -> accountMapper.fromOperation(operation))
                .collect(Collectors.toList());
        return operationDtos;
    }



    @QueryHandler
    public AccountHistoryDto handle(GetAccountHistoryQuery query) {
        Account account = accountRepository.findById(query.getAccountId()).get();
        AccountDto accountDto = accountMapper.fromAccount(account);

        List<Operation> operations = operationRepository.findByAccountId(query.getAccountId());
        List<OperationDto> operationDtos = operations
                .stream()
                .map(operation -> accountMapper.fromOperation(operation))
                .collect(Collectors.toList());
        return new AccountHistoryDto(accountDto, operationDtos);
    }
}
