package com.rc4.query.services;

import com.rc4.coreapi.enums.AccountStatus;
import com.rc4.coreapi.enums.OperationType;
import com.rc4.coreapi.event.AccountActivatedEvent;
import com.rc4.coreapi.event.AccountCreatedEvent;
import com.rc4.coreapi.event.AccountCreditedEvent;
import com.rc4.coreapi.event.AccountDebitedEvent;
import com.rc4.query.dto.AccountDto;
import com.rc4.query.dto.AccountHistoryDto;
import com.rc4.query.dto.OperationDto;
import com.rc4.query.entities.Account;
import com.rc4.query.entities.Operation;
import com.rc4.query.mappers.AccountMapper;
import com.rc4.query.queries.GetAccountByIdQuery;
import com.rc4.query.queries.GetAccountHistoryQuery;
import com.rc4.query.queries.GetAccountOperationQuery;
import com.rc4.query.repository.AccountRepository;
import com.rc4.query.repository.OperationRepository;
import lombok.AllArgsConstructor;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.eventhandling.ResetHandler;
import org.axonframework.queryhandling.QueryUpdateEmitter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class EventHandlerService {
    private final AccountRepository accountRepository;
    private final OperationRepository operationRepository;
    private final QueryUpdateEmitter queryUpdateEmitter;
    private final AccountMapper accountMapper;


    @ResetHandler
    public void resetDatabse(){
        accountRepository.deleteAll();
        operationRepository.deleteAll();
    }

    @EventHandler
    public void on(AccountCreatedEvent event) {
        Account account = new Account();
        account.setId(event.getId());
        account.setBalance(event.getInitialBalance());
        account.setStatus(AccountStatus.CREATED);
        account.setCurrency(event.getCurrency());
        Account savedAccount = accountRepository.save(account);
    }

    @EventHandler
    @Transactional
    public void on(AccountActivatedEvent event) {
        Account account = accountRepository.findById(event.getId()).get();
        account.setStatus(event.getStatus());
        accountRepository.save(account);
    }

    @EventHandler
    @Transactional
    public void on(AccountCreditedEvent event){
        Account account = accountRepository.findById(event.getId()).get();
        Operation operation = new Operation();
        operation.setDate(new Date());
        operation.setAmount(event.getAmount());
        operation.setOperation(OperationType.CREDIT);
        operation.setAccount(account);
        operationRepository.save(operation);
        account.setBalance(account.getBalance().add(event.getAmount()));
        accountRepository.save(account);
        AccountDto accountDto = accountMapper.fromAccount(account);

        OperationDto operationDto = accountMapper.fromOperation(operation);

        AccountHistoryDto accountHistoryDto = new AccountHistoryDto();
        accountHistoryDto.setAccountDto(accountDto);

        queryUpdateEmitter.emit(
                m -> ((GetAccountByIdQuery)m.getPayload()).getAccountId().equals(event.getId()),
                accountDto);
        queryUpdateEmitter.emit(
                m -> ((GetAccountHistoryQuery)m.getPayload()).getAccountId().equals(event.getId()),
                accountHistoryDto);

        queryUpdateEmitter.emit(
                m -> ((GetAccountOperationQuery)m.getPayload()).getAccountId().equals(event.getId()),
                operationDto);
        
    }

    @EventHandler
    @Transactional
    public void on(AccountDebitedEvent event){
        Account account = accountRepository.findById(event.getId()).get();
        Operation operation = new Operation();
        operation.setDate(new Date());
        operation.setAmount(event.getAmount());
        operation.setOperation(OperationType.DEBIT);
        operation.setAccount(account);
        operationRepository.save(operation);
        account.setBalance(account.getBalance().subtract(event.getAmount()));
        accountRepository.save(account);
        AccountDto accountDto = accountMapper.fromAccount(account);
        OperationDto operationDto = accountMapper.fromOperation(operation);

        AccountHistoryDto accountHistoryDto = new AccountHistoryDto();
        accountHistoryDto.setAccountDto(accountDto);

        queryUpdateEmitter.emit(
                m -> ((GetAccountByIdQuery)m.getPayload()).getAccountId().equals(event.getId()),
                accountDto);
        queryUpdateEmitter.emit(
                m -> ((GetAccountHistoryQuery)m.getPayload()).getAccountId().equals(event.getId()),
                accountHistoryDto);

        queryUpdateEmitter.emit(
                m -> ((GetAccountOperationQuery)m.getPayload()).getAccountId().equals(event.getId()),
                operationDto);
    }


}
