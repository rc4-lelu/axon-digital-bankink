package com.rc4.query.mappers;

import com.rc4.query.dto.AccountDto;
import com.rc4.query.dto.OperationDto;
import com.rc4.query.entities.Account;
import com.rc4.query.entities.Operation;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AccountMapper {
    AccountDto fromAccount(Account account);
    Account fromAccountDto(AccountDto accountDto);
    OperationDto fromOperation(Operation operation);
}
