package com.rc4.query.dto;

import com.rc4.coreapi.enums.AccountStatus;
import com.rc4.query.entities.Operation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;
import java.util.List;

@Data @AllArgsConstructor @NoArgsConstructor
public class AccountDto {

    private String id;
    private BigDecimal balance;
    private String currency;
    private AccountStatus status;
}
