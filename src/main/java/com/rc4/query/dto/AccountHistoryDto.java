package com.rc4.query.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data @AllArgsConstructor @NoArgsConstructor
public class AccountHistoryDto {
    private AccountDto accountDto;
    private List<OperationDto> operationDtos;
}
