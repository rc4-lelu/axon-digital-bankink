package com.rc4.query.dto;

import com.rc4.coreapi.enums.OperationType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data @AllArgsConstructor @NoArgsConstructor
public class OperationDto {
    private Long id;
    private Date date;
    private BigDecimal amount;
    private OperationType operation;

}
