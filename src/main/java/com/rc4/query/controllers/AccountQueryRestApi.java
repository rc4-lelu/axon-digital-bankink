package com.rc4.query.controllers;

import com.rc4.query.dto.AccountDto;
import com.rc4.query.dto.AccountHistoryDto;
import com.rc4.query.dto.OperationDto;
import com.rc4.query.queries.GetAccountByIdQuery;
import com.rc4.query.queries.GetAccountHistoryQuery;
import com.rc4.query.queries.GetAccountOperationQuery;
import lombok.AllArgsConstructor;
import org.axonframework.messaging.responsetypes.ResponseType;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.axonframework.queryhandling.SubscriptionQueryResult;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping(path = "/query/accounts")
@AllArgsConstructor
public class AccountQueryRestApi {

    private final QueryGateway queryGateway;

    @GetMapping("/{accountId}")
    public  CompletableFuture<AccountDto> getAccount(@PathVariable String accountId){
        CompletableFuture<AccountDto> query = queryGateway.query(
                new GetAccountByIdQuery(accountId),
                AccountDto.class);
        return query;
    }

    @GetMapping("/{accountId}/operations")
    public  CompletableFuture<List<OperationDto>> getOperationAccount(@PathVariable String accountId){
        CompletableFuture<List<OperationDto>> query = queryGateway.query(
                new GetAccountOperationQuery(accountId),
                ResponseTypes.multipleInstancesOf(OperationDto.class));
        return query;
    }

    @GetMapping("/{accountId}/history")
    public  CompletableFuture<AccountHistoryDto> getHistory(@PathVariable String accountId){
        CompletableFuture<AccountHistoryDto> query = queryGateway.query(
                new GetAccountHistoryQuery(accountId),
                ResponseTypes.instanceOf(AccountHistoryDto.class));
        return query;
    }




    /**
     * SSE : Server Sent EVent
     */
    @GetMapping(path = "/{accountId}/watch", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<AccountDto> subscribeToAccount(@PathVariable String accountId){
        SubscriptionQueryResult<AccountDto, AccountDto> result = queryGateway.subscriptionQuery(
                new GetAccountByIdQuery(accountId),
                ResponseTypes.instanceOf(AccountDto.class),
                ResponseTypes.instanceOf(AccountDto.class));
        return result.initialResult().concatWith(result.updates());
    }

    @GetMapping(path = "/{accountId}/ope", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<OperationDto> subscribeToAccountOp(@PathVariable String accountId){
        SubscriptionQueryResult<OperationDto, OperationDto> result = queryGateway.subscriptionQuery(
                new GetAccountOperationQuery(accountId),
                ResponseTypes.instanceOf(OperationDto.class),
                ResponseTypes.instanceOf(OperationDto.class));
        return result.initialResult().concatWith(result.updates());
    }

    /**
     * SSE : Server Sent EVent
     */
    @GetMapping(path = "/{accountId}/operation", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<AccountHistoryDto> subscribeToHistory(@PathVariable String accountId){
        SubscriptionQueryResult<AccountHistoryDto, AccountHistoryDto> result = queryGateway.subscriptionQuery(
                new GetAccountHistoryQuery(accountId),
                ResponseTypes.instanceOf(AccountHistoryDto.class),
                ResponseTypes.instanceOf(AccountHistoryDto.class));
        return result.initialResult().concatWith(result.updates());

    }
}
