package com.rc4.query.controllers;

import com.rc4.query.services.ReplayEventsService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/query/accounts")
@AllArgsConstructor
public class ReplayEventsController {
    private final ReplayEventsService eventsService;

    @GetMapping("/replayEvents")
    public String replayEvents(){
        eventsService.replay();
        return "Success";
    }
}
