package com.rc4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigitalBankinkApplication {

    public static void main(String[] args) {
        SpringApplication.run(DigitalBankinkApplication.class, args);
    }

}
