package com.rc4.coreapi.command;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@AllArgsConstructor
public abstract class BaseCommand<T>{
    @TargetAggregateIdentifier
    @Getter
    private T id;
}
