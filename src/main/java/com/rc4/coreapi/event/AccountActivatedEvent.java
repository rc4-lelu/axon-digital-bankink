package com.rc4.coreapi.event;

import com.rc4.coreapi.enums.AccountStatus;
import lombok.Getter;

import java.math.BigDecimal;


public class AccountActivatedEvent extends BaseEvent<String> {
    @Getter
    private AccountStatus status ;

    public AccountActivatedEvent(String id, AccountStatus status) {
        super(id);
        this.status = status;
    }
}
