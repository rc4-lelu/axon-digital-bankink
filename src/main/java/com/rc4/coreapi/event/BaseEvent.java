package com.rc4.coreapi.event;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public abstract class BaseEvent<T> {
    @Getter
    private T id;
}
