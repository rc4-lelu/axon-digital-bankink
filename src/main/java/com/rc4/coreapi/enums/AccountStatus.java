package com.rc4.coreapi.enums;

public enum AccountStatus {
    CREATED, ACTIVATED
}
