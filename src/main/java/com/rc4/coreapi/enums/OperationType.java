package com.rc4.coreapi.enums;

public enum OperationType {
    DEBIT, CREDIT
}
